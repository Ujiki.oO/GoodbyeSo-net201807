#!/bin/bash
#	Version: 0.1	2018/07/31 by Ujiki.oO 

SPC="|"
LINE="		--------------------"

if [ "`which awk 2>/dev/null`" = "" ]
then	if [ "`which gawk 2>/dev/null`" = "" ]
	then	echo "＞	エラー	あなたのシステムではａｗｋが利用できません"
		exit
	else	AWK=gawk
	fi
else	AWK=awk
fi

#for A in `find ~/MACRO/HIDEMARU -name "*.MAC" -type f -print | $AWK '{printf("\"%s\"\n",$0)}'`
for A in `find ~/MACRO/HIDEMARU -name "*.MAC" -type f -print`
do	echo "from:	$A"
	B=`echo $A | sed 's/\.MAC$//'`.SED
	echo "  to:	$B"
	echo "$LINE"
	if [ -s $B ]
	then	if [ "`find $B -newer $A -print`" != "" ]
		then	continue
		fi
	fi
	D=0
	IFS=$'\n'
	for C in `cat $A`
	do	if [ "`echo $C | fgrep replaceall`" = "" ] ; then continue ; fi
		D=`expr $D + 1`
		echo $D | $AWK '{printf("%4d\t",$0)}'
		echo $C
		echo $D | $AWK '{printf("%4d\t",$0)}'
		echo $C | $AWK -v SPC="$SPC" -v FS="\"" '{printf("s%s%s%s%s%sg\n",SPC,$2,SPC,$4,SPC)}' | tee -a $B
	done
	echo "$LINE"
	more $B
	echo "$LINE"
done
