#!/bin/bash

CHECKVER="Version:3.1"

VER="$0	Version:4.1 2018/08/04 by Ujiki.oO"
#VER="$0	Version:4.0A 2018/08/03 by Ujiki.oO"
#VER="$0	Version:4.0 2018/08/01 by Ujiki.oO"
#VER="$0	Version:3.1 2018/07/31 by Ujiki.oO"
#VER="$0	Version:3.0 2018/07/31 by Ujiki.oO"
#VER="$0	Version:2.2A 2018/07/29 by Ujiki.oO"
#VER="$0	Version:2.2 2018/07/29 by Ujiki.oO"
#VER="$0	Version:2.1z 2018/07/29 by Ujiki.oO"
#VER="$0	Version:2.0 2018/07/27 by Ujiki.oO"
#VER="$0	Version:1.0 2018/07/25 by Ujiki.oO http://fs4y.com"
REF="https://create2014.blog.so-net.ne.jp/GoodbyeSo-net201807#MtMetadata"
LINE="	------------------------------"

ALLOWCOMMENTS="1"
ALLOWPINGS="1"
PRIMARYCATEGORY="auto"

if [ "$1" = "" -o "$1" = "--help" -o "$1" = "-h" ] ; then echo '

'${VER}'

 USAGE :	'${0}' blogname [ -p | --pre ]				プリプロセッサー	cygwin 対応済み

.or.

 USAGE :	'${0}' blogname						メインプロセッサー
 USAGE :	'${0}' blogname	年月					年月を指定すると再接続通信を行います
									年：4桁　、　月：2桁

【下準備】：

	So-netブログの管理画面 ＞ デザイン ＞ レイアウト で
	左端「レイアウト」の「＋基本コンテンツ」をクリックし
	「記事」をクリックしたままに
	「記事カラム」の一番下に配置する。

	そして、同じ画面の「編集モード」の右側のリンク文字「詳細」をクリックし、
	たった今配置した「記事」から
	「ト」「記」「カ」をクリックして押し込み、
	必ず「設定を保存する」ボタンをクリックする！

	つまり「過去記事」のみで処理させる。

	次に、一番下に配置した「記事」をクリックし
	コンテンツＨＴＭＬ編集を押して
	次のデーターと総てを置き換える！　すべてを置換する！！

<!-- この行から
<hr class="VON" /><h3>	Version:3.1 2018/07/31	</h3><ol>
<% loop:list_article -%><li> DATE: <% article.createstamp | date_format("%m/%d/%Y %H:%M:%S") -%> BASENAME: <% article.page_url | html %> TAGS: <% loop:list_tag %>"<% tag.word | html %>",<% /loop %>
</li><%- /loop -%></ol>
<hr class="ENDE" />
この行まで  -->


 USAGE :	'${0}' blogname --hidemaru		  		秀丸マクロを利用したポストプロセッサー

.and.

 USAGE :	'${0}' blogname -j					結合最終ポストプロセッサー
		'${0}' blogname -j 分割数

'
	exit
fi

if [ "`which awk 2>/dev/null`" = "" ]
then	if [ "`which gawk 2>/dev/null`" = "" ]
	then	echo "＞	エラー	あなたのシステムではａｗｋが利用できません"
		exit
	else	AWK=gawk
	fi
else	AWK=awk
fi

JUNK=$1

if [ "$2" = "-j" -o "$2" = "--join" ]
then
	if [ -d ${JUNK}/${JUNK} ]
	then	WD=${JUNK}/${JUNK}/
	else	if [ -d ${JUNK} ]
		then	WD=${JUNK}/
		else	echo "メインプロセッサーが未処理か指定ミスの様子です。"
			exit
		fi
	fi
	echo ""
	echo "`pwd`/${WD} に存在する月々のエキスポートファイルを連結します。

	連結数がゼロや 1 の場合は１本に連結します。例えば 2 と入力すれば約２等分で２ファイルに連結します。
	例えば 3 と入力すれば約３等分で３ファイルに連結します。
	これはサーバーの能力や通信の能力によって大容量のファイルですとタイムアウトするケースがあるからです。
	タイムアウトすると完全なインポートが出来ていません。インポートした記事を全て削除してから
	連結数を調整して再処理し、より小さな容量でインポートしてください。
	全ての記事を「下書き」にする機能で、完全なインポートが出来るかをテストしてみて下さい。"
	echo ""
	echo -n " Q : 連結数を入力してください。 [1] : "
	read COUNTJ
	if [ "$COUNTJ" = "" ] ; then COUNTJ=1 ; fi
	COUNTJ=`expr 0$COUNTJ \* 1`
	if [ $? != 0 ] ; then exit ; fi
	if [ $COUNTJ -le 1 ] ; then COUNTJ=1 ; fi
	echo "	分割数：	$COUNTJ	で行います。"

	ALLONEFILE=${WD}ALLONE-mtarchive-${1}.log
	Draft="Draft"
	ALLONEFILE2=${WD}ALLONE-mtarchive-${1}_${Draft}.log
	if [ -s $ALLONEFILE -o -s $ALLONEFILE2 -o ! -s $ALLONEFILE -a ! -s $ALLONEFILE2 ]
	then	echo "$LINE"
		if [ -s $ALLONEFILE ] ; then ls -lh ${ALLONEFILE} ; fi
		if [ -s $ALLONEFILE2 ] ; then ls -lh ${ALLONEFILE2} ; fi
		if [ -s $ALLONEFILE -o -s $ALLONEFILE2 ]
		then	echo "$LINE"
			echo -n " Q : 一本化したファイルが既に存在します。再度、一本化しますか？ (y/n) [y] : "
			read yn
		else	yn=y
		fi
		if [ "$yn" != "n" ]
		then
			echo -n " Q : KEYWORDS: を全てブランクにしますか？ (y/n) [n] : "
			read KEYWORDS
			if [ "$KEYWORDS" != "y" ] ; then KEYWORDS="n" ; fi
			if [ $KEYWORDS = "y" ]
			then	echo "	KEYWORDS: を全てブランクにします！"
			else	echo "	KEYWORDS: は  So-netブログが決めた「キーワード」のままです。広告のキーワードを許すのですかね？いえいえ冗談です。"
			fi
			echo -n " Q : 全ての記事を「下書き」にしますか？ (y/n) [n] : "
			read STATUS
			if [ "$STATUS" = "y" ]
			then	STATUS="${Draft}"
				ALLONEFILE=$ALLONEFILE2
			fi
			if [ "$STATUS" = "${Draft}" ]
			then	echo "	全ての記事を「下書き」にします。  検索エンジンに気を配って「公開」にして行きましょう。"
			else	echo "	So-netブログの通り、「下書き」は下書きに、「公開記事」は「公開」されますが、検索エンジンには通知が届かないかも知れませんね。"
			fi
			echo -n " Q : カテゴリー：「未分類」を全ての記事で変更しますか？ (y/n) [n] : "
			read CATEGORY
			if [ "$CATEGORY" = "y" ]
			then	echo -n " Q : カテゴリー：「未分類」を変更する文字を入力してください。リターンだけすると削除となります。 : "
				read CATEGORY_
				if [ "$CATEGORY_" = "" ]
				then	echo "	PRIMARY CATEGORY: は残りますが、CATEGORY: 未分類 は削除します。"
				else	echo "	CATEGORY: 未分類 は、  CATEGORY: ${CATEGORY_} に置換されます。"
				fi
			fi

			echo -n " Q : 実行しますか？ (y/n) [n] : "
			read yn
			if [ "$yn" != "y" ] ; then exit ; fi
			echo -n "まず一本化しています >>>>> $ALLONEFILE "
			echo -n "" > $ALLONEFILE
			for A in `find ${WD}mtarchive-${1}-*.log -maxdepth 0 -type f -print`
			do	IFS=$'\n'
				for B in `cat $A`
				do
					if [ "`echo -n $B | grep ^--------$`" != "" ]
					then	echo -n "."
					fi
					if [ "`echo -n $B | grep ^CATEGORY:\ 未分類$`" != "" ]
					then	if [ "$CATEGORY" = "y" ]
						then	if [ "$CATEGORY_" != "" ]
							then	echo "CATEGORY: "${CATEGORY_} >> $ALLONEFILE
							fi
							continue
						fi
					fi
					if [ "`echo -n $B | grep ^STATUS:\ Publish$`" != "" ]
					then	if [ "$STATUS" = "${Draft}" ]
						then	echo "STATUS: "${Draft} >> $ALLONEFILE
							continue
						fi
					fi
					if [ "`echo -n $B | grep ^KEYWORDS:$`" != "" ]
					then	if [ $KEYWORDS = "y" ]
						then	echo $B >> $ALLONEFILE
							SKIPTOBLOCK="on"
							continue
						fi
					fi
					if [ "$SKIPTOBLOCK" = "on" ]
					then	if [ "`echo -n $B | grep ^-----$`" = "" ]
						then	continue
						else	SKIPTOBLOCK=""
						fi
					fi
					echo $B >> $ALLONEFILE
				done
			done
			echo "作成終了しました。"
			ls -lh $ALLONEFILE
		fi
	fi
	NUM=1
	PARTFILE=${WD}A`echo $NUM | $AWK '{printf("%03d",$1)}'`-mtarchive-${1}.log
	while [ -f $PARTFILE ]
	do	\rm -f $PARTFILE
		NUM=`expr $NUM + 1`
		PARTFILE=${WD}A`echo $NUM | $AWK '{printf("%03d",$1)}'`-mtarchive-${1}.log
	done
	if [ $COUNTJ -gt 1 ]
	then	if [ -s $ALLONEFILE -a -s $ALLONEFILE2 ]
		then	echo " ${ALLONEFILE} と ${ALLONEFILE2} が存在します！"
			echo " 1.	${ALLONEFILE}"
			echo " 2.	${ALLONEFILE2}"
			echo -n " Q : どちらのファイルを分割しますか？数字を入力してください。[1] : "
			read yn
			if [ 0$yn -eq 2 ]
			then	ALLONEFILE=$ALLONEFILE2
			fi
		fi
		if [ ! -s $ALLONEFILE -a -s $ALLONEFILE2 ]
		then	ALLONEFILE=$ALLONEFILE2
		fi
		echo "＞＞＞＞＞	$ALLONEFILE を分割します。"
		COUNTL=`cat $ALLONEFILE | wc -l`
		COUNTL=`expr $COUNTL / $COUNTJ`
		NUM=1
		PARTFILE=${WD}A`echo $NUM | $AWK '{printf("%03d",$1)}'`-mtarchive-${1}.log
		echo -n "" > $PARTFILE
		echo -n "${NUM}): 分割ファイル ${PARTFILE} を作成中 "
		LNUM=1
		IFS=$'\n'
		for A in `cat $ALLONEFILE`
		do	if [ $LNUM -ge $COUNTL ]
			then	if [ "`echo -n $A | grep ^--------$`" != "" ]
				then	echo $A >> $PARTFILE
					echo " 分割出力終了"
					NUM=`expr $NUM + 1`
					PARTFILE=${WD}A`echo $NUM | $AWK '{printf("%03d",$1)}'`-mtarchive-${1}.log
					echo -n "" > $PARTFILE
					echo -n "${NUM}): 分割ファイル ${PARTFILE} を作成中 "
					LNUM=1
					continue
				fi
			fi
			if [ "`echo -n $A | grep ^--------$`" != "" ] ; then echo -n ">" ; fi
			echo $A >> $PARTFILE
			LNUM=`expr $LNUM + 1`
		done
		echo " 分割出力終了"
		echo "$LINE"
		ls -lh ${WD}A*-mtarchive-${1}.log
		if [ -s $ALLONEFILE2 ] ; then ls -lh ${ALLONEFILE2} ; fi
		echo "$LINE"
	fi
	echo $VER
	echo $REF
	exit
fi

if [ ! -d $JUNK ] ; then mkdir $JUNK ; fi

if [ "$2" = "--hidemaru" ]
then	if [ ! -d ~/MACRO/HIDEMARU ]
	then	echo "	ERROR フォルダー「 ~/MACRO/HIDEMARU 」が在りません！"
		exit
	fi
	ls ~/MACRO/HIDEMARU/*.MAC | sed 's/.*\///g' | sed 's/\.MAC$//'
	echo ""
	echo -n "	Q : どのマクロを使用して変換しますか？ : "
	read MACRO
	if [ "$MACRO" = "" ] ; then exit ; fi
	if [ ! -s ~/MACRO/HIDEMARU/${MACRO}.MAC ]
	then	echo "	ERROR	マクロが存在しません！"
		exit
	fi
	if [ ! -s ~/MACRO/HIDEMARU/${MACRO}.SED ]
	then	echo "	ERROR	sed ファイルが存在しません！"
		if [ -s ~/MACRO/HIDEMARU/ConvMacro2Sed.sh ]
		then	echo -n "	ConvMacro2Sed.sh を使って sed ファイルを生成しますか？ [y/n] (y) : "
			read yn
			if [ "$yn" != "n" ]
			then	bash ~/MACRO/HIDEMARU/ConvMacro2Sed.sh
				echo "$LINE"
				ls -l ~/MACRO/HIDEMARU/${MACRO}.SED
				echo "$LINE"
				echo -n "	以上の sed ファイルが出来ています。このまま処理を行いますか？ (y/n) [y] : "
				read yn
				if [ "$yn" = "n" ] ; then exit ; fi
			else	exit
			fi
		fi
	fi
	if [ -s ~/MACRO/HIDEMARU/${MACRO}.SED ]
	then	if [ ! -d $JUNK ]
		then	echo "	メインプロセッサーを先に実行してください。"
			exit
		fi
		JUNK2=${JUNK}/${JUNK} ; if [ ! -d $JUNK2 ] ; then mkdir $JUNK2 ; fi
		for A in `find ${JUNK}/mtarchive-${1}-*.log -type f -print`
		do	echo $A
			cat $A | sed -f ~/MACRO/HIDEMARU/${MACRO}.SED > ${JUNK}/$A
		done
	fi
	exit
fi

if [ "$2" = "-p" -o "$2" = "--pre" ]
then	if [ ! -s mtarchive-${1}.log ]
	then	echo "＞	エラー	「すべて」でエキスポートしたファイル： mtarchive-${1}.log が在りません。"
		echo $VER
		exit
	fi
	if [ ! -d $JUNK ] ; then mkdir $JUNK ; fi
	PREFILE="${JUNK}/FLAG4YOU.dat"
	HTMLFILE1="FLAG4YOU_1.html"
	HTMLFILE2="FLAG4YOU_2.html"
	echo -n "" > $PREFILE
	MES=">>>>>	MAKING A FILE	"
	echo -n "$MES  $PREFILE  	"
	cat mtarchive-${1}.log | $AWK '{if($1 != "DATE:") next;printf("%s\n",$2)}' | $AWK 'BEGIN{FS="/"}{printf("%s-%s\n",$3,$1)}' | sort -u > $PREFILE
	echo "<<<<<"
	if [ -s $PREFILE ]
	then	WCL=`cat $PREFILE | wc -l`
		echo "計：  ${WCL}個の月毎のエキスポートが必要です。"
		echo -n "$MES  $HTMLFILE1  	"
		LANG=C
		WCL=`expr $WCL / 2`
		cat $PREFILE | $AWK -v D="`date`" -v V="$VER" -v R="$REF" -v W=$WCL 'BEGIN{print "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /><title>必要なエキスポート年月</title></head><body>" D "<br />" V "<br /><a href=\"" R "\">" R "</a><ol><table width=100% border=0><tr valign=top><td>";N=0}{N++ ; print "<li><input type=checkbox>" $0 "</li>";if(N == W)print "</td><td>";}END{print "</td></tr></table></ol>" V "<br />チェックボックスはダミーです。<br /><a href=\"" R "\">" R "</a></body></html>";}' > $HTMLFILE1
		echo "<<<<<"
		echo -n "$MES  $HTMLFILE2  	"
		cat $PREFILE | $AWK -v D="`date`" -v V="$VER" -v R="$REF" -v W=$WCL 'BEGIN{print "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /><title>必要なエキスポート年月</title></head><body><iframe sandbox=\"allow-forms\" src=\"https://blog.so-net.ne.jp/MyPage/blog/tools/mt/input#export01-hide-show\" width=\"100%\" height=\"80%\"></iframe>"}{print "<nobr><input type=checkbox>" $0 " </nobr>" ;}END{print "<br />" V "<br /><a href=\"" R "\">" R "</a></body></html>";}' > $HTMLFILE2
		echo "<<<<<"
	fi
	echo $VER
	echo $REF
	exit
fi

if [ "$2" != "" ]
then	YYYYMM=$2
fi

START=`date`

if [ "$PRIMARYCATEGORY" = "auto" ]
then	PRIMARYCATEGORY=`pwd | sed 's|.*/||g'`
fi

for F in `find mtarchive-${1}-*.log -maxdepth 0 -type f -print`
do	A=`echo $F | sed 's/^mtarchive-[^-]*-//' | sed 's/\.log$//' | sed 's/-//'`
	echo "	$F , $A"
	if [ "$YYYYMM" != "" ]
	then	if [ "$YYYYMM" = "$A" ]
		then	if [ ! -d $JUNK ] ; then mkdir $JUNK ; fi
			echo -n "" > ${JUNK}/${A}.html
			touch -t 200001010000 ${JUNK}/${A}.html
		else	echo "	SKIP"
			continue
		fi
	fi
	if [ ! -s ${JUNK}/${A}.html ] ; then touch -t 200001010000 ${JUNK}/${A}.html ; fi
	if [ "`find $F -newer ${JUNK}/${A}.html`" != "" ]
	then	echo	wget -q --no-check-certificate --secure-protocol=TLSv1 --no-cache https://${1}.blog.so-net.ne.jp/archive/${A}-1 -O ${JUNK}/TEMP$$.html
		wget -q --no-check-certificate --secure-protocol=TLSv1 --no-cache https://${1}.blog.so-net.ne.jp/archive/${A}-1 -O ${JUNK}/TEMP$$.html 2>&1 | tee -a ${JUNK}/ErrorWget$$.log
		if [ ! -s ${JUNK}/TEMP$$.html ]
		then	echo "	＞＞エラー通信拒否！	https://${1}.blog.so-net.ne.jp/archive/${A}-1	【処理は中断しました】" | tee -a ${JUNK}/ErrorWget$$.log
			continue
		else	if [ "`cat ${JUNK}/TEMP$$.html | grep -i '</body>'`" = "" ]
			then "	＞＞【警告】タイムアウト！ </body>まで読み切っていません！	https://${1}.blog.so-net.ne.jp/archive/${A}-1	【処理は中断しました】" | tee -a ${JUNK}/ErrorWget$$.log
				continue
			fi
			cat ${JUNK}/TEMP$$.html > ${JUNK}/${A}.html
			touch ${JUNK}/${A}.html
		fi
	fi
	$AWK 'BEGIN{NOWAY="y"}{if($2 == "class=\"ENDE\""){exit}if($2 == "class=\"VON\""){NOWAY="n"}if(NOWAY == "y"){next}print $0}' ${JUNK}/${A}.html > ${JUNK}/${A}.log
	if [ "`head -1 ${JUNK}/${A}.log | fgrep $CHECKVER`" = "" ]
	then	echo " 【[44;32m警告[0m】 So-net側の「個別記事テンプレート」が指定されたフォーマットでないか、または古くないですか？"
	fi
	echo -n "" > ${JUNK}/${F}
	IFS=$'\n'
	KEYON=""
	TAGS=""
	for MT in `cat $F`
	do	if [ "$MT" = "" ] ; then continue ; fi
#		if [ "`echo $MT | fgrep ALLOW\ COMMENTS:`" != "" ]
		if [ "`echo $MT | grep ^ALLOW\ COMMENTS:`" != "" ]
		then	echo "ALLOW COMMENTS: $ALLOWCOMMENTS" >> ${JUNK}/${F}
			continue
		fi
#		if [ "`echo $MT | fgrep ALLOW\ PINGS:`" != "" ]
		if [ "`echo $MT | grep ^ALLOW\ PINGS:`" != "" ]
		then	echo "ALLOW PINGS: $ALLOWPINGS" >> ${JUNK}/${F}
			continue
		fi
#		if [ "`echo $MT | fgrep PRIMARY\ CATEGORY:`" != "" ]
		if [ "`echo $MT | grep ^PRIMARY\ CATEGORY:`" != "" ]
		then	echo "PRIMARY CATEGORY: $PRIMARYCATEGORY" >> ${JUNK}/${F}
			continue
		fi
		if [ "`echo $MT | grep ^--------$`" != "" ]
		then	TAGS="on"
		fi
		if [ "`echo $MT | grep ^COMMENT:$`" != "" ]
		then	TAGS=""
		fi
		if [ "`echo $MT | grep ^PING:$`" != "" ]
		then	TAGS=""
		fi
		echo $MT >> ${JUNK}/${F}
#		if [ "$KEYON" != "" ]
#		then	KEYW=$MT
#			KEYON=""
#			echo "-----
#TAGS:" >> ${JUNK}/${F}
#			if [ "$TG" != "" ]
#			then	echo "$TG" >> ${JUNK}/${F}
#			else	echo "$KEYW" | sed 's/ /,/g' >> ${JUNK}/${F}
#			fi
#		fi
#		if [ "`echo $MT | fgrep DATE:`" != "" ]
		if [ "`echo $MT | grep ^DATE:`" != "" ]
		then	MT=`fgrep "$MT" ${JUNK}/${A}.log`
			if [ "$MT" != "" ]
			then	BN=`echo $MT | $AWK '{print $6}' | $AWK -F / '{print $4}'`
				if [ "$BN" != "" ]
				then	echo "BASENAME: $BN" >> ${JUNK}/${F}
				fi
				if [ "$TAGS" = "on" ]
				then
#					TG=`echo $MT | $AWK '{print $8}'`
					TG="`echo $MT | sed 's/^.* TAGS: //' | sed 's/,$//'`"
#					if [ "$TG" != "" ] ; then
						echo "TAGS: $TG" >> ${JUNK}/${F}
#					fi
				fi
			fi
		fi
#		if [ "$MT" = "KEYWORDS:" ]
#		then	KEYON="on"
#		fi
	done
	if [ ! -s ${JUNK}/${F} ] ; then \rm -f ${JUNK}/${F} ; fi
#	exit
done
echo $LINE
echo "処理開始日時：	$START"
echo "処理終了日時：	`date`"
echo $LINE

if [ -s ${JUNK}/ErrorWget$$.log ]
then	cat ${JUNK}/ErrorWget$$.log >> ${JUNK}/ErrorWget.log
	tail -1000 ${JUNK}/ErrorWget.log > ${JUNK}/ErrorWget$$.log
	cat ${JUNK}/ErrorWget$$.log > ${JUNK}/ErrorWget.log
	echo -n " Q : エラーログファイルを表示しますか？ (y/n) [n] : "
	read yn
	if [ "$yn" = "y" -o "$yn" = "Y" ]
	then	more ${JUNK}/ErrorWget.log
		echo $LINE
	fi
else	echo " ＞＞＞＞＞ 今回はエラーログはありませんでした。おめでとうございます！"
fi
if [ -f ${JUNK}/ErrorWget$$.log ] ; then \rm -f ${JUNK}/ErrorWget$$.log ; fi
echo $VER
echo $REF
exit

【履歴】：
Version:4.1 2018/08/04 by Ujiki.oO
+ TAGS=""
- if [ "`echo $MT | fgrep ALLOW\ COMMENTS:`" != "" ]
+ if [ "`echo $MT | grep ^ALLOW\ COMMENTS:`" != "" ]
- if [ "`echo $MT | fgrep ALLOW\ PINGS:`" != "" ]
+ if [ "`echo $MT | grep ^ALLOW\ PINGS:`" != "" ]
- if [ "`echo $MT | fgrep PRIMARY\ CATEGORY:`" != "" ]
+ if [ "`echo $MT | grep ^PRIMARY\ CATEGORY:`" != "" ]
+ if [ "`echo $MT | grep ^--------$`" != "" ]
+ then	TAGS="on"
+ fi
+ if [ "`echo $MT | grep ^COMMENT:$`" != "" ]
+ then	TAGS=""
+ fi
+ if [ "`echo $MT | grep ^PING:$`" != "" ]
+ then	TAGS=""
+ fi
+ if [ "$TAGS" = "on" ]
+ then
+	TG="`echo $MT | sed 's/^.* TAGS: //' | sed 's/,$//'`"
+	echo "TAGS: $TG" >> ${JUNK}/${F}
+ fi
- if [ "`echo $MT | fgrep DATE:`" != "" ]
+ if [ "`echo $MT | grep ^DATE:`" != "" ]
+	if [ "$MT" != "" ]


Version:4.0 2018/08/01 by Ujiki.oO
+ if [ "$2" = "-j" -o "$2" = "--join" ]

Version:3.1 2018/07/31
+ PRIMARYCATEGORY="auto"
+ if [ "$PRIMARYCATEGORY" = "auto" ]
+ then	PRIMARYCATEGORY=`pwd | sed 's|.*/||g``
+ fi
+ if [ "`echo $MT | fgrep PRIMARY\ CATEGORY:`" != "" ]
+ then	echo "PRIMARY CATEGORY: $PRIMARYCATEGORY" >> ${JUNK}/${F}
+ continue
+ fi

Version:3.0 2018/07/31 by Ujiki.oO
+ USAGE: GetBaseName.sh blogname 年月	年月を指定すると再接続通信を行います
+ if [ "$2" = "--hidemaru" ]

Version:2.2A 2018/07/29 by Ujiki.oO
-#			if [ "$TG" != "" ]
-#			then	echo "TAGS: $TG" >> ${JUNK}/${F}
-#			fi
+			if [ "$TG" != "" ]
+			then	echo "TAGS: $TG" >> ${JUNK}/${F}
+			fi
メタデーターの TAGS: をヘッダーに出力させます。これで「タグ」がインポートされます。

Version:2.2 2018/07/29 by Ujiki.oO
+ if [ ! -s ${JUNK}/${A}.html ] ; then touch -t 200001010000 ${JUNK}/${A}.html ; fi
- if [ ! -s ${JUNK}/${A}.html -o "`find $F -newer ${JUNK}/${A}.html`" != "" ]
+ if [ "`find $F -newer ${JUNK}/${A}.html`" != "" ]
+ wget -q --no-check-certificate --secure-protocol=TLSv1 --no-cache https://${1}.blog.so-net.ne.jp/archive/${A}-1 -O ${JUNK}/${A}.html 2>>&1 | tee -a ${JUNK}/ErrorWget$$.log

Version:2.1z 2018/07/29 by Ujiki.oO
「個別記事」用のテンプレートですが、
ヘルプメッセージの表示とソースの中身を同じ情報にしました。

Version:2.1 2018/07/29 by Ujiki.oO
- <% loop:list_article -%><li> DATE: <% article.createstamp | date_format("%m/%d/%Y %H:%M:%S") -%> BASENAME: <% article.page_url | html %> TAGS: <% loop:list_tag %><% tag.word | html %>,<% /loop %>
+ <% loop:list_article -%><li> DATE: <% article.createstamp | date_format("%m/%d/%Y %H:%M:%S") -%> BASENAME: <% article.page_url | html %> TAGS: <% loop:list_tag %>"<% tag.word | html %>",<% /loop %>
- if [ ! -s ${JUNK}/${A}.html ]
+ if [ ! -s ${JUNK}/${A}.html -o "`find $F -newer ${JUNK}/${A}.html`" != "" ]
- TG=`echo $MT | $AWK '{print $8}'`
+ TG="`echo $MT | sed 's/^.* TAGS: //' | sed 's/,$//'`"
+ else	touch ${JUNK}/${A}.html

Version:2.0 2018/07/27 by Ujiki.oO
+ if [ "$2" = "-p" -o "$2" = "--pre" ]
